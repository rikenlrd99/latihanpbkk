@extends('layouts.app')
@section('title','Food')

@section('main')

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>travels</title>
</head>
<body>
 <p> Makanan khas Palembang berikutnya adalah tekwan. Sama seperti pempek, tekwan juga merupakan makanan olahan daging ikan dan tepung tapioka. Kata tekwan berasal dari bahasa Palembang “berkotek samo kawan” atau duduk mengobrol bersama teman. Tekwan biasaya disajikan dalam bulatan kecil dengan kuah udang yang rasanya khas. 
 Tekwan juga dilengkapi dengan mie sohun, irisan dun bawang, seledri dan bawang goreng. Satu porsi tekwan bisa dibeli mulai dari Rp 15.000.
 </p>
<p>
 <img src="tekwan.jpg" width="500" height="700">
</P>
</body>
</html>

@endsection