@extends('layouts.app')
@section('title','Halaman Category')
@section('main')
@guest
<div class="container">
    <div class="row mt-3 mb-3">
        @foreach($data as $category)
        <div class="col-3">
            <div class="card">
            <div class="card-header">
                <b>{{ $category->name }}</b>
            </div>
            <div class="card-body">
                {{ $category->name }}
            </div>
        </div>
        </div>
        @endforeach
    </div>
</div>
@else
<div class="container">
    <div class="row mt-3 mb-3">
        <a class="btn btn-primary mb-2" href="{{ url('/category/add') }}
        ">Tambah Data</a>
        @foreach($data as $category)
        <div class="col-3">
            <div class="card">
            <div class="card-header">
                <b>{{ $category->name }}</b> 
            </div>
            <div class="card-body">
                {{ $category->name }}
            </div>
            <div class="card-footer">
            <a href="{{ url('/category/edit/'.$category->id) }}" class="btn
              btn-warning btn-sm">Edit</a>
              <a href="{{ url('/category/delete/'.$category->id) }}" class="btn
              btn-danger btn-sm">Hapus</a>
            </div>
        </div>
        </div>
        @endforeach
    </div>
</div>
@endguest
@endsection