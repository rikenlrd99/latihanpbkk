@extends('layouts.app')
@section('title','Halaman news')
@section('main')

<div class="container">
    <div class="row mt-3 mb-3">
        <form action="{{ url('/news/update') }}" method="post">
        @csrf
        <input type="hidden" name="id" value="{{ $news->id }}">
        <div class="mb-3">
            <label>Judul</label>
            <input type="text" class="form-control" name="title" value="{{ $news->title }}">
        </div>
        <div class="mb-3">
            <label>Deskripsi</label>
            <textarea name="description" class="form-control">{{ $news->description }}</textarea>
        </div>
        <div class="mb-3">
            <button class="btn btn-primary">Submit</button>
        </div>
        </form>
    </div>
</div>

@endsection