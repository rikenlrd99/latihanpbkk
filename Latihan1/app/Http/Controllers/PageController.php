<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PageController extends Controller
{

    function home(){
        $angka1 = 2;
        $angka2 = 6;

        $total = $angka1 + $angka2 ;
        return view('welcome')->with(['angka' => $total]);
    }

    function profile(){
        return view('profile021190066');
    }
    
    function news(){
        return view('news021190066');
    }

    function product(){
        return view('product021190066');
    }

    function travel(){
        return view('travels021190066');
    }

    function food(){
        return view('foods021190066');
    }
    function hitung(){
        $angka = 85;

        return view('hitung')->with(['nilai' => $angka]);
    }
}
